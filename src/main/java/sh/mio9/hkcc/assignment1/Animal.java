package sh.mio9.hkcc.assignment1;

public abstract class Animal {
    int legs;
    int age;
    int length;
    boolean ground;
    String color;

    public Animal(int legs, int age, int length, boolean ground, String color) {
        this.legs = legs;
        this.age = age;
        this.length = length;
        this.ground = ground;
        this.color = color;
    }
}
