package sh.mio9.hkcc.assignment1;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TriangleChecker_18140139A {
    public static void main(String[] args) {
        int sideA; //Init with default value
        int sideB;
        int sideC;


        Scanner scanner = new Scanner(System.in); // Create new Scanner object

        System.out.print("Input first side: ");
        sideA = getNextInput(scanner); // get first side (side A)

        System.out.print("Input second side: ");
        sideB = getNextInput(scanner); // get second side (side B)

        System.out.print("Input third side: ");
        sideC = getNextInput(scanner); // get third side (side C)


        Triangle triangle = new Triangle(sideA, sideB, sideC); // Create new Triangle object with 3 sides

        String triangleTypeString = triangle.getType().getName(); //get the type string of the triangle
        System.out.println("The triangle is identified as " + triangleTypeString + " triangle"); // Display the message result

    }

    /**
     * Get next input integer while rejecting invalid inputs and request again
     * @param sc Scanner object created in main method
     * @return int input from user
     */
    private static int getNextInput(Scanner sc) {
        while (true) {
            try {
                return sc.nextInt();
            } catch (InputMismatchException e) {
                sc.next();
                System.out.print("Invalid input, please enter an INTEGER: ");
            }
        }
    }

}

/**
 * Represents .. triangle... what else do you expect?
 */
class Triangle {

    private int a;
    private int b;
    private int c;

    /**
     * Construct a triangle with 3 given sides
     *
     * @param a Side A of the triangle
     * @param b Side B of the triangle
     * @param c Side C of the triangle
     */
    Triangle(int a, int b, int c) {
        // assign constructor parameters to member variable
        this.a = a; // "this" is the object created by this constructor
        this.b = b;
        this.c = c;
    }

    /**
     * Sort the values of sides of the triangle
     *
     * @return Array of sorted value of sides, from shortest to longest
     */
    private int[] getSortedSides() {
        int[] arr = {this.a, this.b, this.c};
        Arrays.sort(arr);
        return arr;
    }

    /**
     * Get the type of triangle
     *
     * @return String of text indicating the type of triangle
     */
    Triangle.Type getType() {
        if (this.isImpossible()) {
            return Type.IMPOSSIBLE;
        }
        if (a == b && b == c) {
            return Type.EQUILATERAL;
        } else if (a == b || a == c || b == c) {
            return Type.ISOSCELES;
        } else {
            if (this.isRightAngled()) {
                return Type.RIGHT_ANGLED;
            } else {
                return Type.SCALENE;
            }
        }
    }

    /**
     * Check if the triangle is a right angled triangle
     *
     * @return TRUE if it is right angled, else FALSE
     */
    private boolean isRightAngled() {
        int[] sides = this.getSortedSides();
        // sides[0] <= sides[1] <= sides[2]
        return Math.sqrt(Math.pow(sides[0], 2) + Math.pow(sides[1], 2)) == sides[2]; //pythagoras theorem

    }

    /**
     * Check if the triangle is an impossible triangle (i.e. having sum of 2 shortest sides smaller than the longest side)
     *
     * @return TRUE if it is an impossible triangle, else FALSE (i.e. the triangle makes sense)
     */
    private boolean isImpossible() {
        int[] sides = this.getSortedSides();
        return sides[0] + sides[1] < sides[2];
    }

    /**
     * Types of identified triangle
     */
    enum Type{

        IMPOSSIBLE(-1,"Impossible"),
        EQUILATERAL(0,"Equilateral"),
        ISOSCELES(1,"Isosceles"),
        RIGHT_ANGLED(2,"Right-angled"),
        SCALENE(3,"Scalene");

        private int code;
        private String name;

        Type(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
}

