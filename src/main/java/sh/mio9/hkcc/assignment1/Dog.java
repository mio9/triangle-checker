package sh.mio9.hkcc.assignment1;

public class Dog extends Animal {
    public Dog(int legs, int age, int length, String color) {
        super(legs, age, length, true, color);
    }
}
