package sh.mio9.hkcc.assignment1;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TriangleTest {

    @Test
    public void testImpossibleTriangle() {

        int a = 1000, b = 10, c = 1;
        System.out.println("Testing impossible triangle with values,");
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        Triangle t = new Triangle(a,b,c);
        System.out.println("type = " + t.getType());
        assertEquals(Triangle.Type.ISOSCELES, t.getType());
    }
}
